import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    private router: Router,
    public authService: AuthService
  ) { }



  ngOnInit(): void {

  }

  navigateHome(){
    this.router.navigate(['/home'])
  }

  navigateLogin(){
    this.authService.logOut();
    this.router.navigate(['/login'])
  }

}
