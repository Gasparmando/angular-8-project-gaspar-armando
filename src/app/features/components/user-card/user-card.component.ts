import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  @Input() user: User;
  @Output() deleteUser = new EventEmitter();


  constructor(private router: Router) { }

  ngOnInit(): void {

  }

 deleteThis(){
   this.deleteUser.emit(this.user.id)
 }

 viewProfile(){
    this.router.navigate(['/user', this.user.id])
}

}
