import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { Store } from '@ngrx/store';
import { UsersState } from '../../store/users.reducers';
import * as UsersAction from 'src/app/features/store/users.actions';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  userList: User[] = [];

  isLoading = true;

  constructor(
    private store: Store<{ users: UsersState}>
  ) { }

  ngOnInit(): void {

    this.store.dispatch(UsersAction.FetchPending());
    this.store.subscribe( ({users}) => {
      this.isLoading = users.pending;
      if(users.data&&users.data.length){
        this.formatUsers(users.data)
      }
    })

  }

  formatUsers(users: any){
    this.userList = users.map( user => {
      return{
        id: user.id,
        name: user.name,
        username: user.username,
        email: user.email,
        address: {
            street: user.address.street,
            suite: user.address.suite,
            city : user.address.city,
            zipcode: user.address.zipcode
        },
        phone: user.phone,
        website: user.website,
        company: user.company
    }

    })
  }

  deleteUser(value:number){
    let id=this.userList.findIndex( user => user.id==value )
    this.userList.splice(id,1)
  }



}
