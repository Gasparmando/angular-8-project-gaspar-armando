import { Injectable } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { map, mergeMap, catchError } from 'rxjs/operators';
import { UsersActionTypes } from './users.actions';
import { of } from 'rxjs';

@Injectable()
export class UsersEffects {

    loadUsers$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(UsersActionTypes.FETCH_PENDING),
            mergeMap(() => this.usersService.getAllUsers().pipe(
                map(users => {
                    return {
                        type: UsersActionTypes.FETCH_FULFILLED,
                        users
                    }
                }),
                catchError(() => of({ type: UsersActionTypes.FETCH_ERROR })))))
    });



    constructor(
        private actions$: Actions,
        private usersService: UsersService
    ) { }

}

