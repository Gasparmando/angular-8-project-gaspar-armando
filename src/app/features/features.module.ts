import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [UserCardComponent, UsersListComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    UsersListComponent
  ]
})
export class FeaturesModule { }
