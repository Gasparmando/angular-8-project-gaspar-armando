import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/features/models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { UsersState } from 'src/app/features/store/users.reducers';

@Component({
  selector: 'app-user-det',
  templateUrl: './user-det.component.html',
  styleUrls: ['./user-det.component.scss']
})
export class UserDetComponent implements OnInit {

  id: number;

  user: User


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<{users: UsersState}>
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      
      params => {
        this.id = parseInt(params.get('id'));
        this.store.subscribe( ({users}) => {
          this.user = users.data.find( user => user.id==this.id)

        })
      }
    );
  }

  goHome() {
    this.router.navigate(['/home'])
  }

}
