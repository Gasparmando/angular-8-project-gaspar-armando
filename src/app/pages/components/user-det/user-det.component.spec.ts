import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetComponent } from './user-det.component';

describe('UserDetComponent', () => {
  let component: UserDetComponent;
  let fixture: ComponentFixture<UserDetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
