import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/features/models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userList: User[];

  constructor() { }

  ngOnInit(): void {
    
  }
}
