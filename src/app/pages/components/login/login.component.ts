import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMsg : Boolean = false;
  
  constructor(
    private router: Router,
    private authService: AuthService
    ){}

  ngOnInit(): void {
  }

  login(user:string, pass:string){

    if( this.authService.logIn(user,pass)){
      this.errorMsg=false
      this.router.navigate(['/home'])
    }else{
      this.errorMsg=true
    }
  }

}
