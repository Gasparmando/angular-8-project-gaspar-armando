import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FeaturesModule } from '../features/features.module';
import { UserDetComponent } from './components/user-det/user-det.component';



@NgModule({
  declarations: [HomeComponent, LoginComponent, NotFoundComponent, UserDetComponent],
  imports: [
    CommonModule,
    FeaturesModule
  ],
  exports: [
    HomeComponent
  ]
})
export class PagesModule { }
